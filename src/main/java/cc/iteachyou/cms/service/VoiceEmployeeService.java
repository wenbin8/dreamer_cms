package cc.iteachyou.cms.service;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.VoiceEmployee;
import com.github.pagehelper.PageInfo;

public interface VoiceEmployeeService {

    void save(VoiceEmployee voiceEmployee);

    PageInfo<VoiceEmployee> listByPage(SearchEntity page);
}

package cc.iteachyou.cms.service.impl;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.dao.PracticalThingsMapper;
import cc.iteachyou.cms.entity.PracticalThings;
import cc.iteachyou.cms.service.PracticalThingsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PracticalThingsServiceImpl implements PracticalThingsService {

    @Resource
    private PracticalThingsMapper practicalThingsMapper;

    @Override
    public void save(PracticalThings voiceEmployee) {
        practicalThingsMapper.insert(voiceEmployee);
    }

    @Override
    public PageInfo<PracticalThings> listByPage(SearchEntity params) {
        if (params.getPageNum() == null || params.getPageNum() == 0) {
            params.setPageNum(1);
        }
        if (params.getPageSize() == null || params.getPageSize() == 0) {
            params.setPageSize(10);
        }
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<PracticalThings> list = practicalThingsMapper.listByPage(params.getEntity());
        PageInfo<PracticalThings> pageList = new PageInfo<>(list);
        return pageList;
    }
}

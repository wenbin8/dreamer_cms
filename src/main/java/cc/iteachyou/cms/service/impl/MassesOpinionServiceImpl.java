package cc.iteachyou.cms.service.impl;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.dao.MassesOpinionMapper;
import cc.iteachyou.cms.entity.MassesOpinion;
import cc.iteachyou.cms.service.MassesOpinionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MassesOpinionServiceImpl implements MassesOpinionService {

    @Resource
    private MassesOpinionMapper massesOpinionMapper;

    @Override
    public void save(MassesOpinion voiceEmployee) {
        massesOpinionMapper.insert(voiceEmployee);
    }

    @Override
    public PageInfo<MassesOpinion> listByPage(SearchEntity params) {
        if (params.getPageNum() == null || params.getPageNum() == 0) {
            params.setPageNum(1);
        }
        if (params.getPageSize() == null || params.getPageSize() == 0) {
            params.setPageSize(10);
        }
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<MassesOpinion> list = massesOpinionMapper.listByPage(params.getEntity());
        PageInfo<MassesOpinion> pageList = new PageInfo<>(list);
        return pageList;
    }
}

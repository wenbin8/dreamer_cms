package cc.iteachyou.cms.service.impl;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.dao.VoiceEmployeeMapper;
import cc.iteachyou.cms.entity.User;
import cc.iteachyou.cms.entity.VoiceEmployee;
import cc.iteachyou.cms.service.VoiceEmployeeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VoiceEmployeeServiceImpl implements VoiceEmployeeService {
	
	@Resource
	private VoiceEmployeeMapper voiceEmployeeMapper;

	@Override
	public void save(VoiceEmployee voiceEmployee) {
		voiceEmployeeMapper.insert(voiceEmployee);
	}

	@Override
	public PageInfo<VoiceEmployee> listByPage(SearchEntity params) {
		if(params.getPageNum() == null || params.getPageNum() == 0) {
			params.setPageNum(1);
		}
		if(params.getPageSize() == null || params.getPageSize() == 0) {
			params.setPageSize(10);
		}
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<VoiceEmployee> list = voiceEmployeeMapper.listByPage(params.getEntity());
		PageInfo<VoiceEmployee> pageList = new PageInfo<>(list);
		return pageList;
	}
}

package cc.iteachyou.cms.service;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.PracticalThings;
import com.github.pagehelper.PageInfo;

public interface PracticalThingsService {

    void save(PracticalThings practicalThings);

    PageInfo<PracticalThings> listByPage(SearchEntity page);
}

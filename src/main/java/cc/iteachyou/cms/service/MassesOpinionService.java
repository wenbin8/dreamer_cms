package cc.iteachyou.cms.service;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.MassesOpinion;
import cc.iteachyou.cms.entity.VoiceEmployee;
import com.github.pagehelper.PageInfo;

public interface MassesOpinionService {

    void save(MassesOpinion voiceEmployee);

    PageInfo<MassesOpinion> listByPage(SearchEntity page);
}

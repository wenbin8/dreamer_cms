package cc.iteachyou.cms.entity;

import cn.hutool.core.date.DateUtil;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 员工之声
 */
@Data
@Table(name = "voice_employee")
public class VoiceEmployee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "feedback")
    private String feedback;

    @Column(name = "ip")
    private String ip;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_time")
    private Date updateTime;

    public String getCreateTimeStr() {
        if (createTime != null) {

        }
        return DateUtil.format(createTime, "yyyy-MM-dd HH:mm:ss");
    }
}
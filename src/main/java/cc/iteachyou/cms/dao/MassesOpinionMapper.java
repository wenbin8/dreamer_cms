package cc.iteachyou.cms.dao;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.MassesOpinion;

import java.util.List;
import java.util.Map;

public interface MassesOpinionMapper extends BaseMapper<MassesOpinion> {

    List<MassesOpinion> listByPage(Map<String, Object> entity);

}
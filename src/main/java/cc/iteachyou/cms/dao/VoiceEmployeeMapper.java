package cc.iteachyou.cms.dao;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.User;
import cc.iteachyou.cms.entity.VoiceEmployee;

import java.util.List;
import java.util.Map;

public interface VoiceEmployeeMapper extends BaseMapper<VoiceEmployee> {

    List<VoiceEmployee> listByPage(Map<String, Object> entity);

}
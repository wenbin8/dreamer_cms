package cc.iteachyou.cms.dao;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.PracticalThings;

import java.util.List;
import java.util.Map;

public interface PracticalThingsMapper extends BaseMapper<PracticalThings> {

    List<PracticalThings> listByPage(Map<String, Object> entity);

}
package cc.iteachyou.cms.controller;

import cc.iteachyou.cms.entity.MassesOpinion;
import cc.iteachyou.cms.entity.PracticalThings;
import cc.iteachyou.cms.entity.VoiceEmployee;
import cc.iteachyou.cms.exception.CmsException;
import cc.iteachyou.cms.exception.FrontException;
import cc.iteachyou.cms.service.MassesOpinionService;
import cc.iteachyou.cms.service.PracticalThingsService;
import cc.iteachyou.cms.service.VoiceEmployeeService;
import cc.iteachyou.cms.utils.IPUtils;
import cc.iteachyou.cms.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("/front/feedback")
public class FrontFeedbackController {

    @Resource
    private VoiceEmployeeService voiceEmployeeService;
    @Resource
    private MassesOpinionService massesOpinionService;
    @Resource
    private PracticalThingsService practicalThingsService;
    /**
     * 员工之声添加
     */
    @PostMapping(value = "add-voice", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Map<String, Object> addVoice(@RequestBody VoiceEmployee voiceEmployee, HttpServletRequest request) throws CmsException {
        Map<String, Object> result = new HashMap<>();
        if (voiceEmployee == null) {
            result.put("success", false);
            result.put("msg", "请填写留言");
            return result;
        }
        if (!StringUtils.hasLength(voiceEmployee.getFeedback())) {
            result.put("success", false);
            result.put("msg", "请填写留言");
            return result;
        }
        if (voiceEmployee.getFeedback().length() > 300) {
            result.put("success", false);
            result.put("msg", "留言不能超过300个字");
            return result;
        }
        try {
            String ipAddr = IPUtils.getIpAddr(request);
            voiceEmployee.setIp(ipAddr);
            voiceEmployee.setId(UUIDUtils.getPrimaryKey());
            voiceEmployee.setUpdateTime(new Date());
            voiceEmployee.setCreateTime(new Date());
            voiceEmployeeService.save(voiceEmployee);
            result.put("success", true);
            result.put("msg", "保存成功");
            return result;
        } catch (Exception e) {
            log.info("func=addVoice voiceEmployee={}", voiceEmployee, e);
            throw new FrontException("500", "系统错误", e.getMessage());
        }
    }

    /**
     * 群众意见收集箱
     */
    @PostMapping(value = "add-masses-opinion", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Map<String, Object> addMassesOpinion(@RequestBody MassesOpinion massesOpinion, HttpServletRequest request) throws CmsException {
        Map<String, Object> result = new HashMap<>();
        if (massesOpinion == null) {
            result.put("success", false);
            result.put("msg", "请填写意见");
            return result;
        }
        if (!StringUtils.hasLength(massesOpinion.getUsername())) {
            result.put("success", false);
            result.put("msg", "请填写姓名");
            return result;
        }
        if (!StringUtils.hasLength(massesOpinion.getPhone())) {
            result.put("success", false);
            result.put("msg", "请填写电话");
            return result;
        }
        if (!StringUtils.hasLength(massesOpinion.getUnit())) {
            result.put("success", false);
            result.put("msg", "请填写工作单位");
            return result;
        }
        if (!StringUtils.hasLength(massesOpinion.getFeedback())) {
            result.put("success", false);
            result.put("msg", "请填写意见");
            return result;
        }
        if (massesOpinion.getFeedback().length() > 300) {
            result.put("success", false);
            result.put("msg", "意见不能超过300个字");
            return result;
        }
        try {
            String ipAddr = IPUtils.getIpAddr(request);
            massesOpinion.setIp(ipAddr);
            massesOpinion.setId(UUIDUtils.getPrimaryKey());
            massesOpinion.setUpdateTime(new Date());
            massesOpinion.setCreateTime(new Date());
            massesOpinionService.save(massesOpinion);
            result.put("success", true);
            result.put("msg", "保存成功");
            return result;
        } catch (Exception e) {
            log.info("func=addMassesOpinion massesOpinion={}", massesOpinion, e);
            throw new FrontException("500", "系统错误", e.getMessage());
        }
    }

    /**
     * 我为群众办实事
     */
    @PostMapping(value = "add-practical-things", produces = "application/json;charset=utf-8")
    @ResponseBody
    public Map<String, Object> addPracticalThings(@RequestBody PracticalThings practicalThings, HttpServletRequest request) throws CmsException {
        Map<String, Object> result = new HashMap<>();
        if (practicalThings == null) {
            result.put("success", false);
            result.put("msg", "请填写意见");
            return result;
        }
        if (!StringUtils.hasLength(practicalThings.getUsername())) {
            result.put("success", false);
            result.put("msg", "请填写姓名");
            return result;
        }
        if (!StringUtils.hasLength(practicalThings.getPhone())) {
            result.put("success", false);
            result.put("msg", "请填写电话");
            return result;
        }
        if (!StringUtils.hasLength(practicalThings.getFeedbackType())) {
            result.put("success", false);
            result.put("msg", "请填写意见类别");
            return result;
        }
        if (!StringUtils.hasLength(practicalThings.getFeedback())) {
            result.put("success", false);
            result.put("msg", "请填写意见");
            return result;
        }
        if (practicalThings.getFeedback().length() > 300) {
            result.put("success", false);
            result.put("msg", "意见不能超过300个字");
            return result;
        }
        try {
            String ipAddr = IPUtils.getIpAddr(request);
            practicalThings.setIp(ipAddr);
            practicalThings.setId(UUIDUtils.getPrimaryKey());
            practicalThings.setUpdateTime(new Date());
            practicalThings.setCreateTime(new Date());
            practicalThingsService.save(practicalThings);
            result.put("success", true);
            result.put("msg", "保存成功");
            return result;
        } catch (Exception e) {
            log.info("func=addPracticalThings practicalThings={}", practicalThings, e);
            throw new FrontException("500", "系统错误", e.getMessage());
        }
    }
}

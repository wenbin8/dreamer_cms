package cc.iteachyou.cms.controller.admin;

import cc.iteachyou.cms.annotation.Log;
import cc.iteachyou.cms.annotation.Log.OperatorType;
import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.MassesOpinion;
import cc.iteachyou.cms.entity.PracticalThings;
import cc.iteachyou.cms.entity.VoiceEmployee;
import cc.iteachyou.cms.service.MassesOpinionService;
import cc.iteachyou.cms.service.PracticalThingsService;
import cc.iteachyou.cms.service.VoiceEmployeeService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

/**
 * 用户反馈 包含：员工之声、群众意见收集箱、我为群众办实事
 */
@Slf4j
@Controller
@RequestMapping("admin/feedback")
public class FeedbackController {

    @Resource
    private VoiceEmployeeService voiceEmployeeService;
    @Resource
    private MassesOpinionService massesOpinionService;
    @Resource
    private PracticalThingsService practicalThingsService;

    @Log(operType = OperatorType.PAGE, module = "员工之声", content = "员工之声")
    @RequestMapping("voice-employees")
    @RequiresPermissions("498jkr41")
    public ModelAndView voiceEmployees(Model model, SearchEntity searchEntity) {
        ModelAndView mv = new ModelAndView();
        PageInfo<VoiceEmployee> page = voiceEmployeeService.listByPage(searchEntity);
        mv.addObject("page", page);
        mv.setViewName("admin/feedback/voiceEmployees");
        return mv;
    }

    /**
     * 群众意见收集箱
     */
    @RequestMapping("masses-opinion")
    @RequiresPermissions("498jkr41")
    public ModelAndView massesOpinion(Model model, SearchEntity searchEntity) {
        ModelAndView mv = new ModelAndView();
        PageInfo<MassesOpinion> page = massesOpinionService.listByPage(searchEntity);
        mv.addObject("page", page);
        mv.setViewName("admin/feedback/massesOpinion");
        return mv;
    }

    /**
     * 我为群众办实事
     */
    @RequestMapping("practical-things")
    @RequiresPermissions("498jkr41")
    public ModelAndView practicalThings(Model model, SearchEntity searchEntity) {
        ModelAndView mv = new ModelAndView();
        PageInfo<PracticalThings> page = practicalThingsService.listByPage(searchEntity);
        mv.addObject("page", page);
        mv.setViewName("admin/feedback/practicalThings");
        return mv;
    }

}
